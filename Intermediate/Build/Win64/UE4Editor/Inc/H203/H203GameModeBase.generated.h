// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef H203_H203GameModeBase_generated_h
#error "H203GameModeBase.generated.h already included, missing '#pragma once' in H203GameModeBase.h"
#endif
#define H203_H203GameModeBase_generated_h

#define H203_Source_H203_H203GameModeBase_h_15_RPC_WRAPPERS
#define H203_Source_H203_H203GameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define H203_Source_H203_H203GameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAH203GameModeBase(); \
	friend struct Z_Construct_UClass_AH203GameModeBase_Statics; \
public: \
	DECLARE_CLASS(AH203GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/H203"), NO_API) \
	DECLARE_SERIALIZER(AH203GameModeBase)


#define H203_Source_H203_H203GameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAH203GameModeBase(); \
	friend struct Z_Construct_UClass_AH203GameModeBase_Statics; \
public: \
	DECLARE_CLASS(AH203GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/H203"), NO_API) \
	DECLARE_SERIALIZER(AH203GameModeBase)


#define H203_Source_H203_H203GameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AH203GameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AH203GameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AH203GameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AH203GameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AH203GameModeBase(AH203GameModeBase&&); \
	NO_API AH203GameModeBase(const AH203GameModeBase&); \
public:


#define H203_Source_H203_H203GameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AH203GameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AH203GameModeBase(AH203GameModeBase&&); \
	NO_API AH203GameModeBase(const AH203GameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AH203GameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AH203GameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AH203GameModeBase)


#define H203_Source_H203_H203GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define H203_Source_H203_H203GameModeBase_h_12_PROLOG
#define H203_Source_H203_H203GameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	H203_Source_H203_H203GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	H203_Source_H203_H203GameModeBase_h_15_RPC_WRAPPERS \
	H203_Source_H203_H203GameModeBase_h_15_INCLASS \
	H203_Source_H203_H203GameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define H203_Source_H203_H203GameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	H203_Source_H203_H203GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	H203_Source_H203_H203GameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	H203_Source_H203_H203GameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	H203_Source_H203_H203GameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> H203_API UClass* StaticClass<class AH203GameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID H203_Source_H203_H203GameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
