// Fill out your copyright notice in the Description page of Project Settings.


#include "Snake.h"
#include "SnakeElementBase.h"
#include "interactable.h"

// Sets default values
ASnake::ASnake()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	MovementSpeed = 10.f;
	LastMoveDirection = EMovementDiretcion::UP;
}

// Called when the game starts or when spawned
void ASnake::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(4);
}

// Called every frame
void ASnake::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();

}

void ASnake::AddSnakeElement(int ElemetsNum)
{
	for (int i = 0; i < ElemetsNum; i++)
	{
		FVector NewLocation(SnakeElement.Num() * ElementSize, 0, 0);
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElem->SnakeOwner = this;
		int32 ElemIndex = SnakeElement.Add(NewSnakeElem);
		if (ElemIndex == 0)
		{
			NewSnakeElem->SetFirstElementType();
		}
	}

}

void ASnake::Move()
{
	FVector MovementVector(ForceInitToZero);

	switch (LastMoveDirection)
	{
	case EMovementDiretcion::UP:
		MovementVector.X += ElementSize;
		break;
	case EMovementDiretcion::DOWN:
		MovementVector.X -= ElementSize;
		break;
	case EMovementDiretcion::LEFT:
		MovementVector.Y += ElementSize;
		break;
	case EMovementDiretcion::RIGHT:
		MovementVector.Y -= ElementSize;
		break;
	}

	//AddActorWorldOffset(MovementVector);
	SnakeElement[0]->ToggleCollision();

	for (int i = SnakeElement.Num() - 1; i>0; i--)
	{
		auto CurrentElement = SnakeElement[i];
		auto PrevElement = SnakeElement[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}

	SnakeElement[0]->AddActorWorldOffset(MovementVector);
	SnakeElement[0]->ToggleCollision();
}

void ASnake::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElement.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		Iinteractable* InteractableInterface = Cast<Iinteractable>(Other);
		if(InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}

